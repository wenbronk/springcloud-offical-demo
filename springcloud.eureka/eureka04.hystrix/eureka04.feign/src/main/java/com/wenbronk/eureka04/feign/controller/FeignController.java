package com.wenbronk.eureka04.feign.controller;

import com.wenbronk.eureka04.feign.service.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@RestController
public class FeignController {

    @Autowired
    private FeignService feignService;

    @RequestMapping("/hello")
    public String getFromFeign(String name) {
        return feignService.getFromFeing(name);
    }
}
