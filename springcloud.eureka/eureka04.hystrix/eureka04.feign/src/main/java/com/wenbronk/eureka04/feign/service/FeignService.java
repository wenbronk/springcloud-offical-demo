package com.wenbronk.eureka04.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@FeignClient(value = "hello.client", fallback = FeignServiceHystrix.class)
public interface FeignService {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    String getFromFeing(@RequestParam("name") String name);

}
