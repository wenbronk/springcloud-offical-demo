package com.wenbronk.eureka04.feign.service;

import org.springframework.stereotype.Component;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@Component
public class FeignServiceHystrix implements FeignService {
    @Override
    public String getFromFeing(String name) {
        return "hello " + name + ", feign got a error from client";
    }
}
