package com.wenbronk.eureka04.ribbon.controller;

import com.wenbronk.eureka04.ribbon.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@RestController
public class RibbonController {

    @Autowired
    private HelloService helloService;

    @RequestMapping("/hi")
    public String getFromClient(String name) {
        return helloService.getHelloFromClient(name);
    }
}
