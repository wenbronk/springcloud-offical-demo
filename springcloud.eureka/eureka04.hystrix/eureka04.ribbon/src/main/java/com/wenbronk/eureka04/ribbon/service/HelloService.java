package com.wenbronk.eureka04.ribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@Service
public class HelloService {

    @Autowired
    @LoadBalanced
    private RestTemplate restTemplate;

    // 添加hystrix
    @HystrixCommand(fallbackMethod = "helloError")
    public String getHelloFromClient(String name) {
        return restTemplate.getForObject("http://hello.client/hello?name=" + name, String.class);
    }

    /**
     * 参数要一致
     */
    public String helloError(String name) {
        return "hello " + name + ", ribbon got a error from client";
    }
}
