package com.wenbronk.eureka05.feign.service;

import org.springframework.stereotype.Component;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@Component
public class FeignServiceHystrix implements FeignService {
    @Override
    public String getFromClient(String name) {
        return "get an error from feign client, params: " + name;
    }
}
