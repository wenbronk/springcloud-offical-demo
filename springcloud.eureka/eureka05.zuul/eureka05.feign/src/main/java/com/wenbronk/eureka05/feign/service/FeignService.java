package com.wenbronk.eureka05.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@FeignClient(value = "hello.service", fallback = FeignServiceHystrix.class)
public interface FeignService {
    @RequestMapping("/hello")
    public String getFromClient(@RequestParam("name") String name);
}
