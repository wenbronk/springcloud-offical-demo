package com.wenbronk.eureka05.ribbon.controller;

import com.wenbronk.eureka05.ribbon.service.RibbonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@RestController
public class RibbonController {

    @Autowired
    private RibbonService ribbonService;

    @RequestMapping("/hi")
    private String getFromClient(@RequestParam  String name) {
        return "ribbon: " + ribbonService.getFromClient(name);
    }

}
