package com.wenbronk.eureka05.ribbon.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@Service
public class RibbonService {

    @Autowired
    @LoadBalanced
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "getError")
    public String getFromClient(String name) {
        return restTemplate.getForObject("http://hello.service/hello?name=" + name, String.class);
    }

    public String getError(String name) {
        return "ribbon get error from client, params: " + name;
    }
}
