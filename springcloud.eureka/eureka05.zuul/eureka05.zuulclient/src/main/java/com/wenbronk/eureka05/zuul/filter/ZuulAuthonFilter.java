package com.wenbronk.eureka05.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@Component
public class ZuulAuthonFilter extends ZuulFilter {
    public static final Logger LOGGER = LoggerFactory.getLogger(ZuulAuthonFilter.class);
    /**
     * pre：路由之前
     * routing：路由之时
     * post： 路由之后
     * error：发送错误调用
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * 过滤顺序
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * 判断是否过滤的逻辑
     * 所有的都过滤
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 过滤的逻辑
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        LOGGER.info(String.format("%s >>> %s", request.getMethod(), request.getRequestURL().toString()));

        String token = request.getParameter("token");

        if (StringUtils.isEmpty(token)) {
            LOGGER.warn("token is empty");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            ctx.setResponseBody("token is empty");
            return null;
        }

        LOGGER.info("token is ok");
        return null;
    }
}
