package com.wenbronk.springcloud.eureka03.feign.controller;

import com.wenbronk.springcloud.eureka03.feign.service.HiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@RestController
public class HiController {

    @Autowired
    private HiService hiService;

    @RequestMapping("/hi")
    public String sayHelloFrom(String name) {
        return hiService.sayHelloFromClient(name);
    }

}
