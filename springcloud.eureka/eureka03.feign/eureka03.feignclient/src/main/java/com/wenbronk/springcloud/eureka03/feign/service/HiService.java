package com.wenbronk.springcloud.eureka03.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@FeignClient(value = "hello.client")
public interface HiService {
    @RequestMapping(value = "/hello", method = {RequestMethod.GET})
    String sayHelloFromClient(@RequestParam("name") String name);
}
