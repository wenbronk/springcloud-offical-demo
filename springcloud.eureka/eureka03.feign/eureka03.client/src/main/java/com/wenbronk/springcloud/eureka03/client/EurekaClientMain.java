package com.wenbronk.springcloud.eureka03.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@SpringBootApplication
@EnableEurekaClient
public class EurekaClientMain {
    public static void main(String[] args) {
        SpringApplication.run(EurekaClientMain.class, args);
    }
}
