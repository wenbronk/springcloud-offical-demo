package com.wenbronk.springcloud.eureka03.client.control;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-07-02
 */
@RestController
public class HelloController {

    @Value("${server.port}")
    private String port;

    @RequestMapping("/hello")
    public String sayHello(String name) {
        System.out.println(name);
        return "hello " + name + ", the port is " + port;
    }
}
