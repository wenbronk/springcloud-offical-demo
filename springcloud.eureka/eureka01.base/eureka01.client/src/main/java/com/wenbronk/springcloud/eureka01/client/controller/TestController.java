package com.wenbronk.springcloud.eureka01.client.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-06-28
 */
@RestController
public class TestController {

    @RequestMapping("/hello")
    public String hellowWorld() {
        return "hello world";
    }

}
