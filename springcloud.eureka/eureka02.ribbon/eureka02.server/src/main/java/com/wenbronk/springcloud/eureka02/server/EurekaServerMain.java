package com.wenbronk.springcloud.eureka02.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Author wenbronk
 * @Date 2019-06-28
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerMain {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServerMain.class);
    }

}
