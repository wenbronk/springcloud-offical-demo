package com.wenbronk.springcloud.eureka02.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-06-28
 */
@RestController
public class TestController {

    @Value("${server.port}")
    private String port;

    @RequestMapping("/hi")
    public String home(String name) {
        System.out.println("eureka client " + name);
        return "hi "+name+",i am from port:" +port;
    }
}
