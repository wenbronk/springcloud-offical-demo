package com.wenbronk.springcloud.eureka02.ribbon.controler;

import com.wenbronk.springcloud.eureka02.ribbon.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author wenbronk
 * @Date 2019-07-01
 */
@RestController
public class HelloContorller {

    @Autowired
    private HelloService helloService;

    @RequestMapping(value = "/hi")
    public String hi(String name){
        String result = helloService.hiService(name);
        return result;
    }

}
