package com.wenbronk.springcloud.eureka02.ribbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author wenbronk
 * @Date 2019-06-28
 * 通过注解 enableDiscoveryClient 向注册中心注册
 */
@EnableEurekaClient
@SpringBootApplication
@EnableDiscoveryClient
public class RibbonMain {
    public static void main(String[] args) {
        SpringApplication.run(RibbonMain.class);
    }
}
