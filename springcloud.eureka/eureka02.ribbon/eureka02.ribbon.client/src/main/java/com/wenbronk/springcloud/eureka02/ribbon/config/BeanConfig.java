package com.wenbronk.springcloud.eureka02.ribbon.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

/**
 * @Author wenbronk
 * @Date 2019-07-01
 */
@Configuration
public class BeanConfig {

    // 通过loadbanlance实现负载均衡
    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Primary
    @Bean
    RestTemplate pri() {
        return new RestTemplate();
    }
}
