package com.wenbronk.springcloud.eureka02.ribbon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @Author wenbronk
 * @Date 2019-07-01
 */
@Service
public class HelloService {

    @Autowired
    @LoadBalanced
    RestTemplate restTemplate;

    public String hiService(String name) {
        String resultValue =  restTemplate.getForObject("http://SERVICE-HI/hi?name="+name,String.class);
        System.out.println(resultValue);
//        String resultValue = "hello world";
        return resultValue;
    }

}
